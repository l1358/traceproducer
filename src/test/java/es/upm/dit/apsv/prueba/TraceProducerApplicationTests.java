package es.upm.dit.apsv.prueba;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import es.upm.dit.apsv.traceproducer.TraceProducerApplication;

@SpringBootTest(classes = TraceProducerApplication.class)
class TraceProducerApplicationTests {

}
